package com.inc.perfectcycle;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import com.inc.exceptions.FalseCycleException;

public class TestPerfectCycle {
	
	@Test
    public void testValidPerfectCycle() throws FalseCycleException {
		PerfectCycle obj = new PerfectCycle(Arrays.asList(3, 0, 1, 2));
	
		boolean actual = obj.start();
		boolean expected = true;
		
        assertEquals(expected, actual);
    }
	
	@Test
    public void testInfiniteCycleWhosValueToIndexToItSelf() throws FalseCycleException {
		PerfectCycle obj = new PerfectCycle(Arrays.asList(3, 0, 1, 2, 4, 2));
	
		boolean actual = obj.start();
		boolean expected = false;
		
        assertEquals(expected, actual);
    }
	
	@Test
    public void testDeadPointIndexInList() throws FalseCycleException {
		PerfectCycle obj = new PerfectCycle(Arrays.asList(0, 2, 5));
	
		boolean actual = obj.start();
		boolean expected = false;
		
        assertEquals(expected, actual);
    }
	
	@Test
    public void testBlankList() throws FalseCycleException {
		PerfectCycle obj = new PerfectCycle();
		
		boolean actual = obj.start();
		boolean expected = true;
		
        assertEquals(expected, actual);
    }
	
	@Test
    public void testThrowsExceptionWhenIndexOutOfRange() {
		PerfectCycle obj = new PerfectCycle(Arrays.asList(1, 9, 0));
	
		FalseCycleException exception = Assertions.assertThrows(FalseCycleException.class, () -> {
			obj.start();
	    });
		
	    String expectedMessage = "List data is not correct";
	    String actualMessage = exception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
    }
}
