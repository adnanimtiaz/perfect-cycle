package com.inc.perfectcycle;

import java.util.Arrays;
import java.util.List;
import com.inc.exceptions.FalseCycleException;

public class MainApplication {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Integer> cycleList = Arrays.asList(3, 0, 1, 2);
		PerfectCycle perfectCycle = new PerfectCycle(cycleList);

		try {
			boolean result = perfectCycle.start();
			
			if(result) {
				System.out.println("True");
			} else {
				System.out.println("False");
			}
		} catch (FalseCycleException e) {
			System.out.println("False");
		}
	}

}
