package com.inc.perfectcycle;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import com.inc.exceptions.FalseCycleException;
import com.inc.interfaces.Cycles;

public class PerfectCycle implements Cycles {
	
	Logger logger = Logger.getLogger(PerfectCycle.class.getName());

	/**
	 * An list to keep cycleList
	 */
	private List<Integer> cycleList;
	
	/**
	 * An list to keep eye on visited index
	 */
	private ArrayList<Integer> visitedIndexs;
	
	/**
	 * Default start index for Recursive call method
	 */
	private int defaultStartCycleIndex;

	public PerfectCycle() {
		this.cycleList = new ArrayList<Integer>();
		this.visitedIndexs = new ArrayList<Integer>();
		this.defaultStartCycleIndex = 0;
	}
	
	public PerfectCycle(List<Integer> cycleList) {
		this.cycleList = cycleList;
		this.visitedIndexs = new ArrayList<Integer>();
		this.defaultStartCycleIndex = 0;
	}
	
	public boolean start() throws FalseCycleException {
		return cycleApply(defaultStartCycleIndex);
	}
	
	/**
	 * Recursive method, call itself according to value based index.
	 * 
	 * @param index 				index value supposed to be visited in list
	 * @throws FalseCycleException  throws on invalid index of list
	 * @return Boolean 				tells about the list contain perfect cycle or not
	 * */
	public boolean cycleApply(int index) throws FalseCycleException {
		// TODO Auto-generated method stub
		try {
			if(cycleList.size() == 0) return true;
			
			int value = cycleList.get(index);

			if(visitedIndexs.size() <= cycleList.size()) {
				logger.log(Level.INFO, "Iteration: Index " + index + ", Value " + value + ", Visited " + visitedIndexs.toString());
				
				// By adding this check, we are removing validateVisitedElements call on each iteration 
				boolean checkAllVisited = false;
				if(visitedIndexs.size() == cycleList.size()) {
					checkAllVisited = validateVisitedElements();
				}
				
				visitedIndexs.add(index);

				if(checkAllVisited && value == cycleList.get(0)) 
					return true;
				else if(index != 0 || value != 0) 
					return cycleApply(value);
			}
			
			return false;
		} catch(ArrayIndexOutOfBoundsException ex) {
			throw new FalseCycleException("List data is not correct");
		}
	}

	/**
	 * Distinct visited index list, by doing this it makes unique values list which should have same size then original list to make it true
	 * 
	 * @return Boolean	It tells all unique index has been visited
	 * */
	public boolean validateVisitedElements() {
		// TODO Auto-generated method stub
		List<Object> distinctArray = visitedIndexs.stream().distinct().collect(Collectors.toList());
		logger.log(Level.INFO, "Distinct " + distinctArray + ", Is " + distinctArray.size());
		
		if(distinctArray.size() == cycleList.size()) return true;
		
		return false;
	}

}
