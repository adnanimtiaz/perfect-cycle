package com.inc.interfaces;

import com.inc.exceptions.FalseCycleException;

public interface Cycles {
	
	boolean cycleApply(int index) throws FalseCycleException;
	boolean validateVisitedElements();
}
