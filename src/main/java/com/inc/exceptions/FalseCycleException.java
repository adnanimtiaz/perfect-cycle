package com.inc.exceptions;

public class FalseCycleException  extends Exception {

	private static final long serialVersionUID = 1L;

	public FalseCycleException(String errorMessage) {
        super(errorMessage);
    }
}
